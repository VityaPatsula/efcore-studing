﻿using Microsoft.EntityFrameworkCore;
using Samurai.Domain;

namespace Samurai.Data
{
    public class SamuraiContext:DbContext
    {
        public DbSet<Samurai.Domain.Samurai> Samurais { get; set; }
        public DbSet<Quote> Quotes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            const string conn = @"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=SamuraisApp;";
            optionsBuilder.UseSqlServer(conn);
        }

    }
}